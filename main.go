package main

import (
	"flag"
	"fmt"
	"log"
)

func main() {
	iFilename := flag.String("if", "", "входной файл")
	oFilename := flag.String("of", "", "выходной файл")
	uri := flag.String("uri", "", "адрес сервера логов")
	convert := flag.Bool("c", false, "режим конвертации")
	iType := flag.String("it", "json", "формат входного файла json/raw")
	oType := flag.String("ot", "web", "формат выхода json/raw/web")
	pos := flag.Int64("pos", 0, "сдвиг начала обработки в байтах")
	iLoc := flag.String("il", "", "временая локация входного файла только для golog")
	flag.Parse()
	if *iFilename == "" {
		log.Println("не указан входной файл")
		return
	}
	if *iFilename == *oFilename {
		log.Println("одинаковый входной и выходной файлы")
		return
	}
	var parser ParserFunc
	switch *iType {
	case "json":
		parser = ParseJSON
	case "raw":
		parser = ParseRaw
	case "golog":
		parser = ParseGolog(*iLoc)
	default:
		log.Println("не верный формат входного файла")
		return
	}
	var handler OutHandler
	switch *oType {
	case "json":
		if *oFilename == "" {
			log.Println("не указан выходной файл")
			return
		}
		handler = OutJSON(*oFilename)
	case "raw":
		if *oFilename == "" {
			log.Println("не указан выходной файл")
			return
		}
		handler = OutRaw(*oFilename)
	case "web":
		if *uri == "" {
			log.Println("не указан адрес лог сервера")
			return
		}
		handler = OutWeb(*uri)
	default:
		log.Println("не верный формат выхода")
		return
	}
	if *convert {
		l, c := ParseFile(*iFilename, *pos, parser, handler)
		log.Printf("обработано %d сообщений\n", c)
		// выводим количество байт в стандартный поток
		fmt.Print(l)
	}
}
