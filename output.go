package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

// OutHandler функция для обработки полученных сообщений
type OutHandler func(m LogMessage) error

// OutWeb отправляет данные на указаный веб адресс в JSON формате
func OutWeb(uri string) OutHandler {
	return func(m LogMessage) error {
		data, err := json.Marshal(m)
		if err != nil {
			log.Println(err)
			return err
		}
		req, err := http.NewRequest(http.MethodPost, uri, bytes.NewBuffer(data))
		if err != nil {
			log.Println(err)
			return err
		}
		req.Header.Set("Content-Type", "application/json")
		res, err := (&http.Client{}).Do(req)
		if err != nil {
			log.Println(err)
			return err
		}
		res.Body.Close()
		if res.StatusCode < 200 || res.StatusCode > 299 {
			err = fmt.Errorf("http code %d", res.StatusCode)
			log.Println(err)
			return err
		}
		return nil
	}
}

// OutJSON сохраняет данные в указаный файл в json формате
func OutJSON(filename string) OutHandler {
	return func(m LogMessage) error {
		file, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0777)
		if err != nil {
			log.Println(err)
			return err
		}
		defer file.Close()
		err = json.NewEncoder(file).Encode(m)
		if err != nil {
			log.Println(err)
			return err
		}
		return nil
	}
}

// OutRaw сохраняет данные в указаный файл в raw формате
func OutRaw(filename string) OutHandler {
	return func(m LogMessage) error {
		file, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0777)
		if err != nil {
			log.Println(err)
			return err
		}
		defer file.Close()
		_, err = fmt.Fprintf(file, "[%d] %s %s [%s] %s\n", m.Work, m.Time.Format(time.RFC3339), m.From, m.Level, m.Message)
		if err != nil {
			log.Println(err)
			return err
		}
		return nil
	}
}
