package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"time"
)

// LogMessage структура для сообщения gojlog
type LogMessage struct {
	Time    time.Time `json:"time"`    // время сообщения
	Work    int64     `json:"work"`    // время работы программы в наносекундах
	From    string    `json:"from"`    // место возникновения сообщения
	Level   string    `json:"level"`   // уровень сообщения
	Message string    `json:"message"` // сообщение
}

// ParserFunc шаблон функции для обработки строк входного файла
type ParserFunc func(row []byte) (LogMessage, error)

// ParseJSON парсит сообщение в формате json
func ParseJSON(row []byte) (m LogMessage, err error) {
	err = json.Unmarshal(row, &m)
	return
}

// ParseRaw парсит сообщение в формате raw
func ParseRaw(row []byte) (m LogMessage, err error) {
	// получаем время работы программы
	start := bytes.IndexRune(row, '[')
	end := bytes.IndexRune(row, ']')
	if !(end > start+1 && start != -1) {
		return m, fmt.Errorf("raw format error: work")
	}
	m.Work, err = strconv.ParseInt(string(row[start+1:end]), 10, 64)
	if err != nil {
		return
	}
	if end+1 > len(row) {
		return m, fmt.Errorf("raw format error: end after work")
	}
	row = row[end+1:]
	// получаем время сообщения
	start = bytes.IndexRune(row, ' ')
	if start != -1 && start < len(row) {
		end = bytes.IndexRune(row[start+1:], ' ')
		// поскольку мы начинаем поиск не с начала строки а со следующего сивола то необходимо сдвинуть на +1
		if end != -1 {
			end++
		}
	} else {
		end = -1
	}
	if !(end > start+1 && start != -1) {
		return m, fmt.Errorf("raw format error: time")
	}
	m.Time, err = time.Parse(time.RFC3339, string(row[start+1:end]))
	if err != nil {
		return
	}
	if end+1 > len(row) {
		return m, fmt.Errorf("raw format error: end after time")
	}
	row = row[end+1:]
	// по месту уровня сообщеия разделяем на
	// откуда, уровень, текст
	start = -1
	switch {
	case bytes.Index(row, []byte(" [debug] ")) != -1:
		l := len(" [debug] ")
		m.Level = "debug"
		start = bytes.Index(row, []byte(" [debug] "))
		end = start + l
	case bytes.Index(row, []byte(" [info] ")) != -1:
		l := len(" [info] ")
		m.Level = "info"
		start = bytes.Index(row, []byte(" [info] "))
		end = start + l
	case bytes.Index(row, []byte(" [warning] ")) != -1:
		l := len(" [warning] ")
		m.Level = "warning"
		start = bytes.Index(row, []byte(" [warning] "))
		end = start + l
	case bytes.Index(row, []byte(" [error] ")) != -1:
		l := len(" [error] ")
		m.Level = "error"
		start = bytes.Index(row, []byte(" [error] "))
		end = start + l
	case bytes.Index(row, []byte(" [critical] ")) != -1:
		l := len(" [critical] ")
		m.Level = "critical"
		start = bytes.Index(row, []byte(" [critical] "))
		end = start + l
	}
	if start == -1 {
		return m, fmt.Errorf("raw format error: level")
	}
	if end+1 > len(row) {
		return m, fmt.Errorf("raw format error: end after level")
	}
	m.From = string(row[:start])
	m.Message = string(row[end:])
	return
}

// ParseGolog парсит сообщение в формате golog
func ParseGolog(loc string) func(row []byte) (m LogMessage, err error) {
	l, err := time.LoadLocation(loc)
	if err != nil {
		log.Println(err)
		l = nil
	}
	return func(row []byte) (m LogMessage, err error) {
		const logTimeFormat = "2006/01/02 15:04:05"
		// получаем код информирующей системы
		start := bytes.IndexRune(row, '[')
		end := bytes.IndexRune(row, ']')
		if !(end > start+1 && start != -1) {
			return m, fmt.Errorf("golog format error: code")
		}
		if end+1 > len(row) {
			return m, fmt.Errorf("raw format error: end after code")
		}
		row = row[end+1:]
		// получаем время сообщения
		start = bytes.IndexRune(row, ' ')
		if start != -1 && start < len(row) {
			end = start + 1 + len(logTimeFormat)
		} else {
			end = -1
		}
		if !(end > start+1 && start != -1) {
			return m, fmt.Errorf("raw format error: time")
		}
		if l != nil {
			m.Time, err = time.ParseInLocation(logTimeFormat, string(row[start+1:end]), l)
		} else {
			m.Time, err = time.Parse(logTimeFormat, string(row[start+1:end]))
		}
		if err != nil {
			return
		}
		if end+1 > len(row) {
			return m, fmt.Errorf("raw format error: end after time")
		}
		row = row[end+1:]
		// по месту уровня сообщения разделяем на
		// откуда, уровень, текст
		start = -1
		switch {
		case bytes.Index(row, []byte(" (DEBUG) ")) != -1:
			l := len(" (DEBUG) ")
			m.Level = "debug"
			start = bytes.Index(row, []byte(" (DEBUG) "))
			end = start + l
		case bytes.Index(row, []byte(" (INFO) ")) != -1:
			l := len(" (INFO) ")
			m.Level = "info"
			start = bytes.Index(row, []byte(" (INFO) "))
			end = start + l
		case bytes.Index(row, []byte(" (WARNING) ")) != -1:
			l := len(" (WARNING) ")
			m.Level = "warning"
			start = bytes.Index(row, []byte(" (WARNING) "))
			end = start + l
		case bytes.Index(row, []byte(" (ERROR) ")) != -1:
			l := len(" (ERROR) ")
			m.Level = "error"
			start = bytes.Index(row, []byte(" (ERROR) "))
			end = start + l
		case bytes.Index(row, []byte(" (CRITICAL) ")) != -1:
			l := len(" (CRITICAL) ")
			m.Level = "critical"
			start = bytes.Index(row, []byte(" (CRITICAL) "))
			end = start + l
		case bytes.Index(row, []byte(" (PANIC) ")) != -1:
			l := len(" (PANIC) ")
			m.Level = "critical"
			start = bytes.Index(row, []byte(" (PANIC) "))
			end = start + l
		}
		if start == -1 {
			return m, fmt.Errorf("raw format error: level")
		}
		m.From = string(row[:start-1]) // -1 для того чтобы срезать : в конце
		if end+1 > len(row) {
			return m, nil // игнорируем, оставляем пустое сообщение
		}
		m.Message = string(row[end:])
		return
	}
}

// ParseRow читает одну строку из ридера и парсит ее
// строка читается до \n после чего парсится parser
// возвращается полученое сообщение и количество прочитаных байт
func ParseRow(r io.ReadSeeker, parser ParserFunc) (m LogMessage, count int, empty bool) {
	buffer := bytes.Buffer{}
	for {
		b := make([]byte, 500)
		c, err := r.Read(b)
		if err != nil {
			if err != io.EOF {
				log.Println(err)
			}
			return LogMessage{}, 0, false
		}
		i := bytes.IndexRune(b, '\n')
		if i > c {
			return LogMessage{}, 0, false
		}
		if i >= 0 {
			buffer.Write(b[:i])
			count += i
			if buffer.Len() == 0 {
				return LogMessage{}, 0, true
			}
			// только для windows обработка переноса каретки
			if i == 1 && buffer.Len() == 1 && b[0] == '\r' {
				return LogMessage{}, 1, true
			}
			m, err = parser(buffer.Bytes())
			if err != nil {
				log.Println(err)
				return LogMessage{}, 0, false
			}
			break
		} else {
			buffer.Write(b)
			count += c
		}
	}
	return
}

// ParseFile парсит файл и для каждого сообщения вызывает обработчик handler
func ParseFile(name string, shift int64, parser ParserFunc, handler OutHandler) (pos int64, handled int64) {
	pos = shift
	f, err := os.Open(name)
	if err != nil {
		log.Println(err)
		return
	}
	stat, err := f.Stat()
	if err != nil {
		log.Println(err)
		return
	}
	// сдвиг позиции если она дальше конца файла
	if pos > stat.Size() {
		log.Println("позиция дальше конца файла, сдвигаю в 0")
		pos = 0
	}
	for {
		f.Seek(pos, 0)
		m, c, empty := ParseRow(f, parser)
		// если это просто перенос строки смещаемся без дальнейшей обработки
		if empty {
			pos += int64(c) + 1
			continue
		}
		if c == 0 {
			return
		}
		err := handler(m)
		if err != nil {
			return
		}
		handled++
		// устанавливаем позицию следующую за \n
		pos += int64(c) + 1
	}
}
