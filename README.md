# Парсер логов для [gojlog](https://gitlab.com/gbh007/gojlog)

## Возможности

- Парс файла лога (raw/json)
- Вывод в raw/json файл или [log-server](https://gitlab.com/gbh007/log-server)
- Работа с позицией в файле
- Вывод последный обработаной позиции в стандартный поток

## Описание аргументов

- `if` входной файл
- `of` выходной файл, только при конвертирование в файл
- `uri` адрес сервера логов, только при конвертирование на log-server
- `c` режим конвертации, по умолчанию выключен
- `it` формат входного файла _json_ или _raw_, по умолчанию _json_
- `ot` формат выхода _json_ или _raw_ или _web_, по умолчанию _web_
- `pos` сдвиг начала файла в байтах, по умолчанию 0

## Примеры использования

Конвертация raw в json

> parser -if input.txt -it raw -of output.json -ot json -c

Конвертация json в raw

> parser -if input.json -it json -of output.txt -ot raw -c

Конвертация raw в web

> parser -if input.txt -it raw -uri <http://localhost:8080/new?author=convert> -ot web -c

Конвертация json в web

> parser -if input.json -it json -uri <http://localhost:8080/new?author=convert> -ot web -c

Конвертация json в web с использованием последней позиции на linux,
**при первом запуске в файле last.pos должен быть указан 0**

> parser -if input.json -it json -uri <http://localhost:8080/new?author=convert> -ot web -c -pos \$(cat last.pos) > last.pos
